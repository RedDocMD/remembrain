import 'package:flutter/material.dart';
import 'package:remembrain/create_reminder.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:remembrain/sign_in.dart';

void main() {
  runApp(MainApp());
}

class MainApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Remembrain',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MainPage(),
    );
  }
}

class MainPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainPageState();
}

class MainPageState extends State<MainPage> {
  static const _title = "Remembrain";

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          title: Text(_title),
          bottom: const TabBar(
            tabs: [
              Tab(
                icon: Icon(Icons.add),
              ),
              Tab(
                icon: Icon(Icons.list),
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            CreateReminderPage(),
            Text("bye"),
          ],
        ),
      ),
    );
  }
}
