import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';

class Reminder {
  String name;
  DateTime date;
  TimeOfDay time;
  Uuid uuid = Uuid();

  Reminder(this.name, this.date, this.time) {
    uuid.v4();
  }

  @override
  String toString() =>
      "Reminder: {Name: ${this.name}, Date: ${this.date}, Time: ${this.time}}";
}
