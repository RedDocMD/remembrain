import 'package:flutter/material.dart';
import 'package:remembrain/reminder.dart';
import 'package:remembrain/sign_in.dart';

class CreateReminderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => CreateReminderPageState();
}

class CreateReminderPageState extends State<CreateReminderPage> {
  final _nameController = TextEditingController();
  var _reminderDate = DateTime.now();
  final _reminderDateController = TextEditingController();
  var _reminderTime = TimeOfDay.now();
  final _reminderTimeController = TextEditingController();

  @override
  void dispose() {
    _nameController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _reminderDateController.text =
        "${_reminderDate.day}/${_reminderDate.month}/${_reminderDate.year}";
    _reminderTimeController.text = _reminderTime.format(context);
    return ListView(
      children: [
        Padding(
          padding: EdgeInsets.only(
            top: 15,
          ),
        ),
        Center(
          child: Text(
            "Create Reminder",
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 32,
              color: Colors.black.withAlpha(200),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
        ),
        Container(
          child: TextField(
            controller: _nameController,
            decoration: InputDecoration(
              hintText: "\"Thing to Do\"",
              helperText: "Name",
              border: OutlineInputBorder(),
            ),
          ),
          padding: EdgeInsets.fromLTRB(30, 20, 30, 20),
        ),
        Row(
          children: [
            Expanded(
              child: Container(
                child: TextField(
                  controller: _reminderDateController,
                  onTap: () async {
                    final date = await showDatePicker(
                      context: context,
                      initialDate: _reminderDate,
                      firstDate: DateTime.now(),
                      lastDate: DateTime.utc(2100, 12, 31),
                    );
                    if (date != null) {
                      _reminderDate = date;
                    }
                    _reminderDateController.text =
                        "${_reminderDate.day}/${_reminderDate.month}/${_reminderDate.year}";
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    helperText: "Date",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(30, 0, 20, 0),
              ),
            ),
            Expanded(
              child: Container(
                child: TextField(
                  controller: _reminderTimeController,
                  onTap: () async {
                    final time = await showTimePicker(
                      context: context,
                      initialTime: _reminderTime,
                    );
                    if (time != null) {
                      _reminderTime = time;
                    }
                    _reminderTimeController.text =
                        _reminderTime.format(context);
                  },
                  readOnly: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    helperText: "Time",
                  ),
                ),
                padding: EdgeInsets.fromLTRB(20, 0, 30, 0),
              ),
            ),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(
            top: 50,
          ),
        ),
        RawMaterialButton(
          onPressed: () {
            final reminder =
                Reminder(_nameController.text, _reminderDate, _reminderTime);
            print(reminder);
            var account = handleSignIn();
          },
          child: Icon(
            Icons.add,
            size: 40.0,
          ),
          elevation: 2.0,
          fillColor: Colors.blue,
          shape: CircleBorder(),
          padding: EdgeInsets.all(7),
        ),
      ],
    );
  }
}
