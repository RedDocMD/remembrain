import 'package:googleapis/calendar/v3.dart';
import 'package:google_sign_in/google_sign_in.dart';

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    CalendarApi.calendarEventsScope,
  ],
);

Future<GoogleSignInAccount?> handleSignIn() async {
  try {
    var account = await _googleSignIn.signIn();
    return account;
  } catch (error) {
    // FIXME: Don't squelch this error
    print(error);
    return null;
  }
}

void handleSignInSilently() {
  _googleSignIn.signInSilently();
}
